
#ifndef PWM_TINY2313A_DEFINE_H
#define PWM_TINY2313A_DEFINE_H

#define TRUE ((char)(1))
#define FALSE ((char)(0))

#define byte char

#define MAX_BYTE sizeof(byte)

#endif /*PWM_TINY2313A_DEFINE_H*/
