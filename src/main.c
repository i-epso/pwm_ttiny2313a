#include <stdlib.h>
#include <avr/io.h>
#include "global.h"
#include <avr/pgmspace.h>
#include "avr/interrupt.h"

const char vector[3] PROGMEM = {1, 2, 3};

ISR(TIMER1_OVF_vect)
{
    PORTB = ~(PINB << (int)vector[1]);
}

/**
 * @brief Main entry point
 * @return
 */
int main()
{
    DDRB = 0xFF;
    PORTB = 0xFF;

    while (TRUE)
    {
         /* main code here*/
    }

    return EXIT_SUCCESS;
}
